![](https://img.shields.io/badge/version-0.2.4-blue?style=for-the-badge)
![](https://img.shields.io/badge/PHP-8.0-green?style=for-the-badge)

# Creators: PHP-Common

<b>Creators: PHP-Common</b> - пакет, который вмещает в себя общие функции всех микросервисов.

## Функционал

- Утилиты
    - Проверка и обработка исключений
    - Сообщения исключений
    - Оболочка над [Guzzle](https://laravel.com/docs/8.x/http-client)
    - Организация query-запросов [QueryBuilder](#query-builder)
- Микросервисное ПО
    - Планировщик для реестра микросервисов
    - Система общего сервисного логирования
    - Базовая обработка файлов
    - Базовый сервис для обработки уровней доступа
    - Перечисление списка внутремикросервисных имен
- Промежуточное ПО
    - Обработка уровней доступа (см. подробнее [API Gateway: Уровни доступа](https://gitlab.com/Wedyarit/api-gateway#уровень-доступа))
    - Обработка UUID пользователей

## Содержание

- [Установка](#установка)
- [Система общего логирования](#система-общего-логирования)
    - [Информационные сообщения](#info-log)
    - [Сообщения о предупреждениях](#warn-log)
    - [Сообщения об ошибках](#error-log)
- [Реестр микросервисов](#реестр-микросервисов)
    - [Heartbeat (сердцебиение)](#heartbeat)
    - [Адреса микросервисов](#адреса-микросервисов)
- [Работа с файлами](#работа-с-файлами)
    - [Загрузка файла](#загрузка-файла)
    - [Скачивание файла](#скачивание-файла)
    - [Удаление файла](#удаление-файла)
- [QueryBuilder](#query-builder)

## Установка

1. Создайте `ssh` ключи для `git`. Подробнее узнать [здесь](https://docs.gitlab.com/ee/ssh/).
2. Перейдите в `composer.json` Вашего проекта и скопируйте ссылку на репозиторий пакета:
    ```json
    "repositories": [{
            "type" : "vcs",
            "url"  : "git@gitlab.com:creators.prod.house/php-common.git"
    }],
    ```
3. Установите пакет через [composer](https://getcomposer.org/):

    ```bash
    $ composer require creators/php-common
    ```

4. Перейдите в `bootstrap/app.php` и замените стандартный `Handler` на `phpcommon/Handler/Handler::class`:

    ```php
    $app->singleton(
        Illuminate\Contracts\Debug\ExceptionHandler::class,
        phpcommon\Handler\Handler::class
    );
    ```

5. Зарегистрируйте глобальные Middleware в `bootstrap/app.php` (при необходимости)
    ```php
   $app->middleware([
        phpcommon\Middleware\AccessLevelMiddleware::class,
        phpcommon\Middleware\UserMiddleware::class
   ]);
    ```

6. Зарегистрируйте Schedule для Heartbeat в app/Console/Kernel.php:
   ```php
   $schedule->call(function () {
       Scheduler::heartbeat();
   })->everyMinute();
   ```
   _Не забудьте импортировать Scheduler (use phpcommon\Utils\Scheduler)_

## Система общего логирования

Логирование ошибок, предупреждений и информационных сообщений осуществляется посредством [Notifications-Microservice](https://gitlab.com/miyron666/muz-notification) (Микросервиса уведомлений) в чат Telegram.

### Info Log

Для вывода информационного сообщения, используйте

```php
\phpcommon\Utils\ServiceLog::info('<Service-Name>', '<Message_Content>');
```

### Warn Log

Для вывода сообщения о предупреждении, используйте

```php
\phpcommon\Utils\ServiceLog::warn('<Service-Name>', '<Message_Content>');
```

### Error Log

Для вывода сообщения об ошибке, используйте

```php
\phpcommon\Utils\ServiceLog::error('<Service-Name>', '<Message_Content>');
```

## Реестр микросервисов

### Heartbeat

Heartbeat - (с англ. "сердцебиение") ежеминутный запрос, подаваемый каждым микросервисом на реестр микросервисов. Сердцебиение предназначено для логирования и мониторинга статуса микросервисов и архитектуры в целом. Для планированных запросов используется Schedule. Пример использования:

   ```php
   $schedule->call(function () {
       Scheduler::heartbeat();
   })->everyMinute();
   ```

### Адреса микросервисов

Согласно парадигмам микросервисной архитектуре, каждый микросервис обладает динамическим адресом, что придает гибкости всей системе.

Получение адресов микросервисов осуществляется посредством [Microservice-Registry](https://gitlab.com/Wedyarit/microservice-registry) (Реестра микросервисов).
<br>В случае, если реестр микросервисов вернет ошибку (будет оффлайн), пакет будет использовать кэшированные адреса микросервисов, поэтому убедитесь, что memcached на микросервисе сконфигурирован корректно.

```php
\phpcommon\Utils\ServiceRegistry::getAddress('<Service-Name>');
```

## Работа с файлами

Работа с файлами осуществляется посредством [Files-Microservice](https://gitlab.com/Wedyarit/files-microservice) (Микросервиса файлов)

### Загрузка файла

```php
\phpcommon\Utils\File::upload('<File>', '<File-Name>', 'Optional<Delete-At>');
```

### Скачивание файла

Будет возвращена ссылка для скачивания файла

```php
\phpcommon\Utils\File::download('<UUID>');
```

### Удаление файла

```php
\phpcommon\Utils\File::delete('<UUID>');
```

## Query Builder

Конструктор запросов - утилита, предназначенная для фильтрации, пагинации, сортировки возвращаемых запросом данных. Конструктор принимает несколько статичных параметров.

### Колонки

Позволяют отбирать определенные колонки модели. В случае указания некорректной колонки, будет выведена соответствующее исключение.

Параметр: **columns**

- /users?columns=email,uuid,first_name

```json
"data": {
"email": "Emanuel.Schowalter@hotmail.com",
"uuid": "113eb3b3-2554-464d-9224-62e61c930f1a"
"first_name": "Emanuel"
}
```

### Колонки

Позволяют отбирать определенные колонки модели. В случае указания некорректной колонки, будет выведено соответствующее исключение.
В случае отсутсвия параметра, будут возвращены все колонки, доступные пользователю согласно его уровню доступа. (см. подробнее [API Gateway: Уровни доступа](https://gitlab.com/Wedyarit/api-gateway#уровень-доступа))

Параметр: **columns**

- /users?columns=email,uuid,first_name

```json
{
  "email": "Emanuel.Schowalter@hotmail.com",
  "uuid": "113eb3b3-2554-464d-9224-62e61c930f1a"
  "first_name": "Emanuel"
}
```
<br>

Подобный функционал также осуществим и со связями-зависимостями.
- /users?columns=email,social_networks.name,social_networks.url

```json
{
  "email": "Emanuel.Schowalter@hotmail.com",
  "social_networks": [
    {
      "name": "Telegram",
      "url": "https://telegram.org"
    }
  ]
}
```


### Включения

Позволяют отбирать определенные зависимости модели. В случае указания некорректной связи, будет выведено соответствующее исключение.

Параметр: **includes**

- /users?includes=social_networks

```json
{
  "email": "Emanuel.Schowalter@hotmail.com",
  "uuid": "113eb3b3-2554-464d-9224-62e61c930f1a"
  "first_name": "Emanuel",
  "social_networks": [
    {
      "id": 1,
      "name": "Twitter",
      "url": "https://twitter.com/"
    }
  ]
}
```


### Лимиты

Лимиты позволяют определять количество записей на странице. Значение по умолчанию: 15 записей.

Параметр: **limit**

- /users?limit=5

### Страницы

Применяется с лимитами, определяет номер страницы. Позволяет разделять большой объем данных на страницы с фиксированным количеством записей на каждой. Значение по умолчанию: 15 записей.

Параметр: **page**

- /users?page=1

### Сортировка

Позволяет определять порядок записей. 

Параметр: **order_by**

- /users?order_by=id:asc,uuid:desc

### Фильтрация

Позволяет фильтровать входные данные. 

Параметр: **динамический**
Ключом параметра является название колонки.

Примеры запросов фильтрации:
- /users?first_name=Emanuel
- /users?first_name!=Emanuel
- /users?first_name=*uel
- /users?first_name=Em*
- /users?first_name=\*an*
- /users?balance=1.546
- /users?balance=<2.55
- /users?balance=>1.699
- /users?balance<2.45
- /users?balance>1.324
- /users?first_name=[null]
- /users?first_name!=[null]


<br>
<img src="logo.svg" alt="logo" width="150px"> 


