<?php

namespace phpcommon\http\Messages;

class ENTITY_DELETED_Message extends Message
{
    public function __construct($details = '')
    {
        parent::__construct('ENTITY_DELETED', 'Entity has been successfully deleted', 200, $details);
    }
}
