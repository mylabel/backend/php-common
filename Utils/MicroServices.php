<?php

namespace phpcommon\Utils;

abstract class MicroServices
{
    const FILES_MICROSERVICE = 'files_microservice';
    const NOTIFICATIONS_MICROSERVICE = 'notifications_microservice';
    const AUTH_MICROSERVICE = 'auth_microservice';
    const API_GATEWAY_MICROSERVICE = 'api_gateway_microservice';
    const TICKETS_MICROSERVICE = 'tickets_microservice';
    const RANDOMCOVER_MICROSERVICE = 'randomcover_microservice';
    const BOOKING_MICROSERVICE = 'booking_microservice';
    const PAYMENT_MICROSERVICE = 'payments_microservice';
    const MYLABEL_MICROSERVICE = 'mylabel_microservice';
    const SUBSCRIPTIONS_MICROSERVICE = 'subscriptions_microservice';
    const MUSLINK_MICROSERVICE = 'muslink_microservice';
    const ARTISTS_MICROSERVICE = 'artists_microservice';
}