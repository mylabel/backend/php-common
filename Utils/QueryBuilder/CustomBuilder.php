<?php

namespace phpcommon\Utils\QueryBuilder;

use Illuminate\Database\Query\Builder;

class CustomBuilder extends \Illuminate\Database\Eloquent\Builder
{
    public function __construct(Builder $query)
    {
        parent::__construct($query);
    }

    public function queryBuilder($request): QueryBuilder
    {
        return new QueryBuilder($this, $request);
    }

    public function accessLevel(int $accessLevel, string $operationType): static
    {
        $this->model->setHidden($this->model::fields);
        $this->model->fillable = [];

        if ($accessLevel < 0 || $accessLevel > max(array_keys($this->model::accessLevels[$operationType]))) {
            $this->model->fillable($this->model::fields);
            $this->model->setVisible($this->model::fields);
            $this->model->makeVisible($this->model::fields);

            return $this;
        }

        $this->model->fillable($this->model::accessLevels[$operationType][$accessLevel]);
        $this->model->makeVisible($this->model::accessLevels[$operationType][$accessLevel]);
        $this->model->setVisible($this->model::accessLevels[$operationType][$accessLevel]);

        return $this;
    }
}
